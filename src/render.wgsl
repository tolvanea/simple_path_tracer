// Copy image from texture.
// Inspired from https://github.com/googlefonts/compute-shader-101/tree/main/compute-shader-toy


@group(0) @binding(0)
var inputTex: texture_2d<f32>;

// This is a known hack to draw screen quad without passing any vertex buffer. Even though there are
// no vertices given, the vertex shader can be still invoked with a draw command on the rust-side.
// We invoke it three times to compute vertices of a triangle from indices 0, 1, 2. We
// make this triangle so big that it fills the whole screen by using side length double to
// the screen. Screen coordinates in between will be interpolated by the pipeline so that the
// fragment shader receives coordinates on range [-1,1].
@vertex
fn vs_main(@builtin(vertex_index) vertex_idx: u32) -> @builtin(position) vec4<f32> {
    // The three vertex indices passed are [0, 1, 2], which in binary are [00, 01, 10]
    let x = f32(vertex_idx & 1u);  // yields x coordinates: [0, 1, 0]
    let y = f32(vertex_idx >> 1u); // yields y coordinates: [0, 0, 1]
    // Triangle that spans x and y with [-1, 3] just barely fills the screen rectangle [-1, 1]
    return vec4<f32>(4.0*x - 1.0, 4.0*y - 1.0, 0.0, 1.0);
}

@fragment
fn fs_main(@builtin(position) screen_coord: vec4<f32>) -> @location(0) vec4<f32> {
    let pixel = textureLoad(inputTex, vec2<i32>(screen_coord.xy), 0);
    return vec4<f32>(clamp(pixel.xyz / pixel.w, vec3<f32>(0.0), vec3<f32>(1.0)), 1.0);
}
