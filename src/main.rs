// Inspired from
//  https://github.com/googlefonts/compute-shader-101/tree/main/compute-shader-toy
//  https://github.com/adamnemecek/shadertoy/tree/main/src

#![allow(clippy::needless_return)]
mod window_boilerplate;
mod wgpu_boilerplate;

use wgpu::{
    Buffer, BindGroupLayout, BindGroup, RenderPipeline, ComputePipeline, RenderBundle, Texture
};
use bytemuck::{Pod, Zeroable};
use std::mem::size_of;
use winit::event::{WindowEvent, VirtualKeyCode};
use std::collections::HashSet;


use instant::Instant;
use glam::{Mat3A, UVec2, Vec3, Vec4};  // Vectors of f32

use wgpu_boilerplate::{WgpuDriver, Boilerplate as _};

// Set custom resolution for image that can be larger than your screen. Value 'None' means that
// image resolution will be your window's resolution. The 'Some' detaches the image size from window
#[cfg(not(target_arch = "wasm32"))]
const IMAGE_RESOLUTION: Option<UVec2> =  // width, height
    None;
//     Some(glam::const_uvec2!([3840, 2160]));
//     Some(glam::const_uvec2!([2560, 1440]));
//     Some(glam::const_uvec2!([1920, 1080]));
//     Some(glam::const_uvec2!([960, 540]));

#[cfg(target_arch = "wasm32")]
const IMAGE_RESOLUTION: Option<UVec2> = None;

#[repr(C)]
#[derive(Clone, Copy, Pod, Zeroable)]
struct Vertex([f32; 4]);

// This struct represents the state in shader side, that is camera orientation and time.
// It will be send to GPU on every frame. Struct fields are ordered so that they do not 
// have any badding between in wgsl. For example, Vec3 has align 4*4B, not align 3*4B, where
// 4B is size of f32. So Struct{Vec3, Vec3} take as much space as Struct{Vec3, f32, Vec3, f32}.
#[repr(C)]
#[derive(Clone, Copy, Debug)]
struct CameraUniform {
    rotation_matrix: Mat3A,  // Camera rotation. Padding: 48. Columns: x, y and z axis for camera
    position: Vec3,          // Camera position
    time: f32,               // Time in seconds, used for seed
    resolution: UVec2,       // Image xy resolution, not necessarily screen resolution
    focus: f32,              // Focus distance
    aperture: f32,           // Aperture radius, depth of field, "bokeh"
    v: Vec4,                 // Freely tunable variables for shader tweaking
}

#[derive(Clone, Copy, Debug)]
struct GameState {
    uniform: CameraUniform,  // Parameters that are needed on shader side
    theta: f32,              // Up-down camera rotation
    phi: f32,                // Left-right camera rotation
    timestep: f32,           // Timestep between frame draws
}

#[derive(Clone, Copy, Debug)]
struct Counter {
    clock_start: Instant,     // From beginning of program
    clock_interval: Instant,  // From beginning of 100 frames interval
    clock_frame: Instant,     // From beginning of last frame
    frames_start: usize,      // Frames since beginning of program
    frames_interval: usize,   // Frames since interval, so that at 100 it is reseted
}

struct WgpuObjects {
    uniform_buf: Buffer,
    images: [Texture; 2],
    image_read_buf: Buffer,
    compute_bind_group_layout: BindGroupLayout,
    render_bind_group_layout: BindGroupLayout,
    compute_bind_groups: [BindGroup; 2],
    compute_pipeline: ComputePipeline,
    render_pipeline: RenderPipeline,
    render_bundles: [RenderBundle; 2],
}

struct Application {
    gpu: WgpuObjects,         // buffers, shaders, pipelines, etc
    state: GameState,         // Game state fields
    counter: Counter,         // For reporting framerate every 100 frames
    key_events: (HashSet<VirtualKeyCode>, HashSet<VirtualKeyCode>),
    image_resolution: UVec2,  // Is either 'IMAGE_RESOLUTION' or window resolution. Same as uniform
    //image_pending: bool,   // If image print button P was pressed, this is set temporarily true
}

// With `Pod` we ensure that data can be transferred easily to GPU.
// There must not be any memory layout padding between struct members.
unsafe impl Pod for CameraUniform {}
unsafe impl Zeroable for CameraUniform {}

impl GameState {
    fn default_state(screen_resolution: UVec2) -> Self {
        let mut state: Self = Default::default();
        state.uniform.resolution = screen_resolution;
        return state;
    }
    fn move_cam(&mut self, multiplier: f32, dir: Vec3) {
        let step: Vec3 = dir * multiplier * self.timestep;
        self.uniform.position += step;
    }
    fn rotate(&mut self, multiplier: f32, up_right: bool) {  // true means up, false means right
        use std::f32::consts::PI;
        if up_right {
            self.theta += multiplier * self.timestep;    // pitch, up-down
            self.theta = self.theta.clamp(-PI/2.0, PI/2.0);
        } else {
            self.phi += multiplier * self.timestep;        // yaw, left-right
            self.phi = self.phi.rem_euclid(2.0*PI);
        };
        let pitch = Mat3A::from_rotation_x(self.theta);
        let yaw   = Mat3A::from_rotation_y(self.phi);
        self.uniform.rotation_matrix = yaw * pitch; // Imagine invisible '* identity_matrix'
    }
}
impl Default for GameState {
    fn default() -> GameState {
        // Uniform represents camera orientation, written to buffers each frame
        let uniform = CameraUniform {
            rotation_matrix: Mat3A::IDENTITY,     // Camera orientation
            position: Vec3::new(0.2, 0.5, 1.4),   // Camera position
            time: 0.0,                            // Time in seconds
            resolution: UVec2::new(300, 200), // Image resolution
            focus: 0.8,                           // Focus distance
            aperture: 0.005,                      // Aperture radius, depth of field, "bokeh"
            v: Vec4::ONE,                         // Tunable variables
        };
        return GameState {
            uniform,
            theta: 0.0,                          // Up-down rotation in radians
            phi: 0.0,                            // Left-right rotation in radians
            timestep: 0.02,                       // Timestep between frame draws
        };
    }
}
impl Default for Counter {
    fn default() -> Counter {
        let now = Instant::now();
        return Counter {
            clock_start: now,
            clock_interval: now,       // For printing fps every 100 frames
            clock_frame: now,
            frames_start: 0,
            frames_interval: 0,
        };
    }
}

impl Application {
    /// Move camera
    fn process_keyboard_inputs(&mut self) -> (bool, bool) {
        use VirtualKeyCode::*;
        let s = &mut self.state;
        let r = s.uniform.rotation_matrix;
        let (pressed, released) = &mut self.key_events;

        let ctrl = pressed.contains(&LControl) || pressed.contains(&RControl); // Multiply <-> Add
        let f = if pressed.contains(&RShift) {0.2} else {1.0};  // Fine control with right shift
        let m = 1.0 + 0.2*f;  // multiplier
        let decrease = |x: &mut f32| if ctrl {*x -= 0.1*f;} else {*x /= m;};
        let increase = |x: &mut f32| if ctrl {*x += 0.1*f;} else {*x *= m;};

        let mut events_without_action = 0;
        let mut print_image = false;

        for key in pressed.iter() {
            let mut moved = true;
            match key {
                // Move camera
                W      => { s.move_cam(-f, r.col(2).into()); },
                A      => { s.move_cam(-f, r.col(0).into()); },
                S      => { s.move_cam( f, r.col(2).into()); },
                D      => { s.move_cam( f, r.col(0).into()); },
                Space  => { s.move_cam( f, Vec3::new(0.0, f, 0.0)); },  // Move up
                LShift => { s.move_cam(-f, Vec3::new(0.0, f, 0.0)); },  // Move down
                Up     => { s.rotate( f, true); },
                Left   => { s.rotate( f, false); },
                Down   => { s.rotate(-f, true); },
                Right  => { s.rotate(-f, false); },
                // Tune camera lens parameters
                Q      => { decrease(&mut s.uniform.focus) },
                E      => { increase(&mut s.uniform.focus) },
                Z      => { decrease(&mut s.uniform.aperture) },
                C      => { increase(&mut s.uniform.aperture) },
                // Free variables for debugging/tweaking shaders shader parameters
                V      => { decrease(&mut s.uniform.v.x) },
                F      => { increase(&mut s.uniform.v.x) },
                B      => { decrease(&mut s.uniform.v.y) },
                G      => { increase(&mut s.uniform.v.y) },
                N      => { decrease(&mut s.uniform.v.z) },
                J      => { increase(&mut s.uniform.v.z) },
                M      => { decrease(&mut s.uniform.v.w) },
                K      => { increase(&mut s.uniform.v.w) },
                _ => { moved = false },
            }
            // Print data for certain debug buttons
            match key {
                Q | E | Z | C => {
                    let u = s.uniform;
                    eprintln!("Focus: {:3.8}, Aperture: {:3.8},", u.focus, u.aperture);
                },
                V | F | B | G | N | J | M | K  => {
                    let u = s.uniform;
                    eprintln!("v: [{:3.8}, {:3.8}, {:3.8}, {:3.8}]", u.v.x, u.v.y, u.v.z, u.v.w);
                },
                _ => (),
            }
            if moved { continue; }

            // The keys below are not meant to be pressed continuously, but rather as strokes
            events_without_action += 1;
            match key {
                // Print image
                P => { print_image = true; }
                // The rest is debug stuff
                I => { dbg!(&s); },                                                // Print state
                O => { let u = s.uniform; dbg!(u.position, u.rotation_matrix); },  // Print camera
                // Reset view orientation and other parameters
                R => {
                    let res = s.uniform.resolution;
                    *s = GameState::default();
                    s.uniform.resolution = res;
                    events_without_action -= 1;  // Unlike others, this action requires redraw
                },
                _ => {},
            }
            released.insert(*key); // Mark these non-continuously-pressed keys to be removed
        }
        let did_something = events_without_action < pressed.len();
        released.iter().for_each(|key| {pressed.remove(key);});
        released.clear();
        return (did_something, print_image);
    }

    fn update_time(&mut self) {
        let c = &mut self.counter;
        self.state.uniform.time = c.clock_start.elapsed().as_secs_f32();
        self.state.timestep = c.clock_frame.elapsed().as_secs_f32().min(0.1);  // TODO Remove min
        c.clock_frame = Instant::now();

        if c.frames_interval == 100 {
            println!(
                "Frame: {:>4}ms    Total: {:>5}/{}s",
                c.clock_interval.elapsed().as_millis() / 100,
                c.frames_start,
                self.state.uniform.time as u32
            );
            c.clock_interval = c.clock_frame;
            c.frames_interval = 0;
        }
    }
}




impl window_boilerplate::RenderLoop for Application {
    fn init(driver: &WgpuDriver, screen_resolution: UVec2) -> Self {
        let res = IMAGE_RESOLUTION.unwrap_or(screen_resolution);
        // Initialize the compute shader, which does the heavy lifting of path tracing.
        // Cumulation of rays is achieved with two alternating textures. Color of previous frame is
        // read from one texture and written to another texture along with the added color
        // calculated in current frame. The read/write roles of the two textures are swapped in
        // the next frame. This is done with two textures instead of one because of gpu limitations.

        // Reserve memory on the device side as "buffers". No information is passed there yet.
        let uniform_buf = driver.create_uniform_buffer(size_of::<CameraUniform>());
        let imgs = [  // The first one is initially written with zeros, the last one is read from.
            driver.create_screen_buffer(res, wgpu::TextureUsages::COPY_DST),
            driver.create_screen_buffer(res, wgpu::TextureUsages::COPY_SRC),
        ];
        // Host-mappaple buffer in which to copy texture img[1]
        let image_read_buf = driver.create_mappable_buffer(res);
        driver.initialize_screen_buffer_with_zeros(&imgs[0], res);

        // Prefixes "compute" and "render" refers to the two associated shaders below

        // BindGroupLayout defines an interface for a shader. That is, it defines the types for
        // buffers that do input and output. Does not contain the shader nor affiliated buffers.
        let compute_bgl = driver.create_compute_bind_group_layout(size_of::<CameraUniform>());
        // PipelineLayout wraps one or more BindGroupLayout's. It's redundant here with just one.
        let compute_pll = driver.create_compute_pipeline_layout(&compute_bgl);
        // BindGroup is almost the same as BindGroupLayout, but it also wraps the buffer in itself.
        let compute_bgs = [
            driver.create_compute_bind_group(&compute_bgl, &uniform_buf, &imgs[0], &imgs[1]),
            driver.create_compute_bind_group(&compute_bgl, &uniform_buf, &imgs[1], &imgs[0]),
        ];
        let compute_shader = driver.create_compute_shader();
        // Pipeline contains the shader with some information of its input/outputs
        let compute_pl = driver.create_compute_pipeline(compute_pll, &compute_shader);


        // Initialize the render shader, which does only one thing: draws the texture on the screen
        let render_bgl = driver.create_render_bind_group_layout();
        let render_bgs = [
            driver.create_render_bind_group(&render_bgl, &imgs[1]),
            driver.create_render_bind_group(&render_bgl, &imgs[0]),
        ];
        let render_pll = driver.create_render_pipeline_layout(&render_bgl);
        let render_shader = driver.create_render_shader();
        let render_pl = driver.create_render_pipeline(render_pll, &render_shader);
        // The render buffers (bindgroups) and shaders (pipelines) are combined into a bundle,
        // which will be executed on every frame. This same thing is not done on compute shader,
        // because compute bundles are not (yet) supported, so they are re-combined on each frame.
        let render_bundles = [
            driver.create_render_bundle(&render_bgs[0], &render_pl),
            driver.create_render_bundle(&render_bgs[1], &render_pl),
        ];

        let gpu = WgpuObjects {
            uniform_buf,
            images: imgs,
            image_read_buf,
            compute_bind_group_layout: compute_bgl,
            render_bind_group_layout: render_bgl,
            compute_bind_groups: compute_bgs,
            compute_pipeline: compute_pl,
            render_pipeline: render_pl,
            render_bundles,
        };

        Application {
            gpu, // WebGPU boilerplate
            state: GameState::default_state(res),
            counter: Default::default(),
            key_events: (HashSet::new(), HashSet::new()),
            image_resolution: res,
        }
    }

    fn window_event(&mut self, _: &WgpuDriver, event: WindowEvent) {
        use winit::event::{
            WindowEvent::KeyboardInput as Key,
            ElementState::{Pressed, Released},
            KeyboardInput as Input,
        };

        #[allow(clippy::single_match)]  // TODO remove when mouse events arrive
        match event {
            Key {input: Input { virtual_keycode: Some(key), state: keystate, ..}, ..}
                => match keystate {
                    Pressed  => { self.key_events.0.insert(key); },
                    Released => { self.key_events.1.insert(key); },
                }
            _ => (),
        }
    }

    fn resize(&mut self, driver: &WgpuDriver, screen_resolution: UVec2) {
        // If imafe size depends on resolution, recompute render bundle with the new texture size.
        if IMAGE_RESOLUTION.is_none() {
            self.state.uniform.resolution = screen_resolution;
            self.image_resolution = screen_resolution;

            self.counter.frames_start = 0;
            self.counter.frames_interval = 0;
            let g = &mut self.gpu;
            let imgs = [
                driver.create_screen_buffer(screen_resolution, wgpu::TextureUsages::COPY_DST),
                driver.create_screen_buffer(screen_resolution, wgpu::TextureUsages::COPY_SRC),
            ];
            driver.initialize_screen_buffer_with_zeros(&imgs[0], screen_resolution);
            g.image_read_buf = driver.create_mappable_buffer(screen_resolution);
            let render_bgs = [
                driver.create_render_bind_group(&g.render_bind_group_layout, &imgs[1]),
                driver.create_render_bind_group(&g.render_bind_group_layout, &imgs[0]),
            ];
            g.render_bundles = [
                driver.create_render_bundle(&render_bgs[0], &g.render_pipeline),
                driver.create_render_bundle(&render_bgs[1], &g.render_pipeline),
            ];
            g.compute_bind_groups = [
                driver.create_compute_bind_group(
                    &g.compute_bind_group_layout, &g.uniform_buf, &imgs[0], &imgs[1]
                ),
                driver.create_compute_bind_group(
                    &g.compute_bind_group_layout, &g.uniform_buf, &imgs[1], &imgs[0]
                ),
            ];
            g.images = imgs;
        }
    }

    fn render(&mut self, driver: &WgpuDriver) {
        self.update_time();
        let (redraw_screen, print_keyevent) = self.process_keyboard_inputs();
        if redraw_screen {
            driver.initialize_screen_buffer_with_zeros(&self.gpu.images[0], self.image_resolution);
            self.counter.frames_start = 0;
            self.counter.frames_interval = 0;
        }

        let f = self.counter.frames_start + 1;  // Print frames at power of 2
        let print_image = print_keyevent || f > 32 && (f & (f - 1)) == 0;
        let texture_and_buffer = if print_image {
            Some((&self.gpu.images[1], &self.gpu.image_read_buf))
        } else {
            None
        };


        driver.write_buffer(&self.gpu.uniform_buf, &self.state.uniform);
        let i = self.counter.frames_start % 2;
        driver.render(
            &self.gpu.compute_pipeline,
            &self.gpu.compute_bind_groups[i],
            &self.gpu.render_bundles[i],
            self.image_resolution,
            texture_and_buffer,
        );

        if print_image {
            let time = self.counter.clock_start.elapsed().as_secs();
            let c = if print_keyevent {"print"} else {"save"};
            driver.read_texture_to_png(
                &self.gpu.image_read_buf,
                self.image_resolution,
                &format!("{}frames_{}secs_{}", f, time, c),
            );
        }

        self.counter.frames_start += 1;
        self.counter.frames_interval += 1;

    }
}

fn main() {
    // Will call RenderLoop::init once, and then call RenderLoop::render in a loop.
    window_boilerplate::start_event_loop::<Application>("Raymarching");
}

