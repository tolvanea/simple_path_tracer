#![deny(unused_must_use)]

use wgpu::{
    Buffer, Texture, BindGroupLayout, BindGroup, PipelineLayout, ShaderModule, RenderPipeline,
    ComputePipeline, RenderBundle, BindingResource,
};
use winit::window::Window;
use glam::UVec2;
#[cfg(not(target_arch = "wasm32"))]
use image::{save_buffer, ColorType::Rgb8};  // Saving PNG-image

use std::num::NonZeroU32;
use std::mem::size_of;

/// GPU Device, screen canvas, etc
pub struct WgpuDriver {
    surface: wgpu::Surface,
    queue: wgpu::Queue,
    device: wgpu::Device,
    config: wgpu::SurfaceConfiguration,
}

// This applies to texture on which image is written. 4 * size_of(f32) = 4 * 4 = 16
const BYTES_PER_PIXEL: usize = size_of::<[f32; 4]>() as usize;

impl WgpuDriver {
    /// # Safety
    /// Window reference must outlive the returned object
    pub async fn init(window: &Window, res: UVec2) -> WgpuDriver {
        log::info!("Initializing the hardware...");
        log::info!("Initializing the surface...");

        let backend = wgpu::util::backend_bits_from_env().unwrap_or_else(wgpu::Backends::all);
        let instance = wgpu::Instance::new(backend);

        // Raw Window Handle must be a valid object to create a surface upon and must remain
        // valid for the lifetime of the returned surface.
        let surface = unsafe { instance.create_surface(window) };

        let mut adapters = Vec::new();
        let mut names = Vec::new();
        for adapter in instance.enumerate_adapters(wgpu::Backends::PRIMARY) {
            let info = adapter.get_info();
            adapters.push(adapter);
            names.push(format!("{} ({:?})", info.name, info.backend))
        }

        let args: Vec<String> = std::env::args().collect();
        let select_gpu = args.len() > 1 && args[1] == "select";
        if args.len() > 1 && args[1] != "select" {
            println!("Unknown argument '{}'. The only valid argument is 'select'.", args[1]);
        }

        let adapter = if select_gpu {
            #[cfg(not(target_arch = "wasm32"))]
            {
                use dialoguer::{theme::ColorfulTheme, Select};
                let selection = Select::with_theme(&ColorfulTheme::default())
                    .with_prompt("Select gpu")
                    .default(0)
                    .items(&names)
                    .interact()
                    .unwrap();
                println!("Selected {}", names[selection]);
                adapters.swap_remove(selection)
            }
            #[cfg(target_arch = "wasm32")]
            instance.request_adapter(Default::default())
        } else {
            println!("Found GPU's:");
            for adapter in names {
                println!("    {}", adapter);
            }
            // If environment variable WGPU_ADAPTER_NAME is given, use that GPU, else use default
            let adapter = match wgpu::util::initialize_adapter_from_env(&instance, backend) {
                Some(adapter) => {
                    println!("Chose the following GPU based on environment variable:");
                    println!("    {} ({:?})", adapter.get_info().name, adapter.get_info().backend);
                    adapter
                },
                None => {
                    let adapter = instance.request_adapter(
                        &wgpu::RequestAdapterOptionsBase{  // Use dedicated GPU over integrated
                            power_preference: wgpu::PowerPreference::HighPerformance,
                            ..Default::default()
                        }
                    ).await.expect("No suitable GPU adapters found on the system!");
                    println!("Selected GPU:");
                    println!("    {} ({:?})", adapter.get_info().name, adapter.get_info().backend);
                    println!("You can choose the gpu by adding command line argument 'select'.");
                    adapter
                },
            };
            adapter
        };

        let required_downlevel_capabilities = wgpu::DownlevelCapabilities {
            flags: wgpu::DownlevelFlags::empty(),
            shader_model: wgpu::ShaderModel::Sm5,
            ..Default::default()
        };
        let downlevel_capabilities = adapter.get_downlevel_capabilities();
        assert!(
            downlevel_capabilities.shader_model >= required_downlevel_capabilities.shader_model,
            "Adapter does not support the required minimum shader model {:?}",
            required_downlevel_capabilities.shader_model
        );
        assert!(
            downlevel_capabilities.flags.contains(required_downlevel_capabilities.flags),
            "Adapter does not support the required downlevel capabilities {:?}",
            required_downlevel_capabilities.flags - downlevel_capabilities.flags
        );

        let trace_dir = std::env::var("WGPU_TRACE");
        let (device, queue) = adapter
            .request_device(&Default::default(), trace_dir.ok().as_ref().map(std::path::Path::new))
            .await
            .expect("Unable to find a suitable GPU adapter!");

        let config = wgpu::SurfaceConfiguration {
            usage: wgpu::TextureUsages::RENDER_ATTACHMENT,
            format: surface.get_supported_formats(&adapter)[0],
            width: res.x,
            height: res.y,
            present_mode: wgpu::PresentMode::Fifo,
            alpha_mode: wgpu::CompositeAlphaMode::Auto,
        };
        surface.configure(&device, &config);

        return WgpuDriver {
            surface,
            device,
            queue,
            config,
        }
    }

    pub fn resize(&mut self, res: UVec2) {
        log::info!("Resizing to {:?}", (self.config.width, self.config.height));
        self.config.width = res.x.max(1);
        self.config.height = res.y.max(1);
        self.surface.configure(&self.device, &self.config);
    }
}

/// No need for trait here, but this is just to list all the boilerplate required to initialize
/// and run the GPU shaders.
pub trait Boilerplate {
    fn create_uniform_buffer(&self, size: usize) -> Buffer;
    fn create_screen_buffer(&self, res: UVec2, tu: wgpu::TextureUsages) -> Texture;
    fn create_mappable_buffer(&self, res: UVec2) -> Buffer;
    fn initialize_screen_buffer_with_zeros(&self, t: &Texture, res: UVec2);
    fn create_compute_bind_group_layout(&self, uniform_size: usize) -> BindGroupLayout;
    fn create_render_bind_group_layout(&self) -> BindGroupLayout;
    fn create_compute_pipeline_layout(&self, bgl: &BindGroupLayout) -> PipelineLayout;
    fn create_render_pipeline_layout(&self, bgl: &BindGroupLayout) -> PipelineLayout;
    fn create_compute_bind_group(
        &self, bgl:
        &BindGroupLayout,
        u: &Buffer,
        t1: &Texture,
        t2: &Texture
    ) -> BindGroup;
    fn create_render_bind_group(&self, bgl: &BindGroupLayout, t: &Texture) -> BindGroup;
    fn create_compute_shader(&self) -> ShaderModule;
    fn create_render_shader(&self) -> ShaderModule;
    fn create_compute_pipeline(&self, rpll: PipelineLayout, s: &ShaderModule) -> ComputePipeline;
    fn create_render_pipeline(&self, rpll: PipelineLayout, s: &ShaderModule) -> RenderPipeline;
    fn create_render_bundle(&self, bg: &BindGroup, rpl: &RenderPipeline) -> RenderBundle;
    fn write_buffer<T: bytemuck::Pod>(&self, buf: &Buffer, data: &T);
    fn read_texture_to_png(&self, image_read_buf: &Buffer, res: UVec2, postfix: &str);
    fn render(
        &self,
        cpl: &ComputePipeline,
        cbg: &BindGroup,
        rb: &RenderBundle,
        res: UVec2,
        texture_read: Option<(&Texture, &Buffer)>
    );
}

impl Boilerplate for WgpuDriver {
    fn create_uniform_buffer(&self, size: usize) -> Buffer {
        return self.device.create_buffer(&wgpu::BufferDescriptor {
            label: Some("Uniform buffer containing parameters"),
            size: size as u64,
            usage: wgpu::BufferUsages::UNIFORM | wgpu::BufferUsages::COPY_DST,
            mapped_at_creation: false,
        });
    }

    fn create_screen_buffer(&self, res: UVec2, tu: wgpu::TextureUsages) -> Texture {
        // The dimensions are known, but they are required for symmetry with 'create_uniform_buffer'
        //assert!(width == self.config.width && height == self.config.height, "Wrong dimensions");
        return self.device.create_texture(&wgpu::TextureDescriptor {
            label: Some("Screen image passed from compute shader to vertex shader"),
            size: wgpu::Extent3d {width: res.x, height: res.y, depth_or_array_layers: 1},
            mip_level_count: 1,
            sample_count: 1,
            dimension: wgpu::TextureDimension::D2,
            format: wgpu::TextureFormat::Rgba32Float,  // The texture is f32
            usage: wgpu::TextureUsages::STORAGE_BINDING | wgpu::TextureUsages::TEXTURE_BINDING | tu,
        });
    }

    //  "Mapping" means copying GPU buffer to CPU memory. It has to be "unmapped" before use on GPU
    fn create_mappable_buffer(&self, res: UVec2) -> Buffer {
        let bytes_per_row = res.x as usize * BYTES_PER_PIXEL;
        let bytes_per_row_padded = (((bytes_per_row - 1) / 256) + 1) * 256;
        return self.device.create_buffer(&wgpu::BufferDescriptor {
            label: Some("Copy image from GPU to CPU to write png"),
            size: bytes_per_row_padded as u64 * res.y as u64,
            usage: wgpu::BufferUsages::MAP_READ | wgpu::BufferUsages::COPY_DST,
            mapped_at_creation: false,
        });
    }

    fn initialize_screen_buffer_with_zeros(&self, t: &Texture, res: UVec2) {
        // By the way, the fourth index, alpha channel, is not used as transparency but as ray count
        self.queue.write_texture(
            t.as_image_copy(),
            // By IEEE 754, if bits are zero, the floating point is 0.0. So below it's the same as:
            // let v = vec![[0.0f32, 0.0, 0.0, 0.0]; (width * height) as usize];
            // unsafe{&std::slice::from_raw_parts(v.as_ptr() as *const _, (v.len()*BYTES) as usize)}
            &vec![0u8; BYTES_PER_PIXEL * (res.x * res.y) as usize],
            wgpu::ImageDataLayout {
                offset: 0,
                bytes_per_row: Some(NonZeroU32::new(res.x * BYTES_PER_PIXEL as u32).unwrap()),
                rows_per_image: None,
            },
            wgpu::Extent3d {width: res.x, height: res.y, depth_or_array_layers: 1},
        );
    }

    fn create_compute_bind_group_layout(
        &self,
        uniform_size: usize,
    ) -> BindGroupLayout {
        return self.device.create_bind_group_layout(&wgpu::BindGroupLayoutDescriptor {
            label: Some("create_compute_bind_group_layout"),
            entries: &[
                wgpu::BindGroupLayoutEntry {  // Uniform, camera orientation parameters
                    binding: 0,
                    visibility: wgpu::ShaderStages::COMPUTE,
                    ty: wgpu::BindingType::Buffer {
                        ty: wgpu::BufferBindingType::Uniform,
                        has_dynamic_offset: false,
                        min_binding_size:
                            wgpu::BufferSize::new(uniform_size as u64),
                    },
                    count: None,
                },  // Read old state from this texture
                wgpu::BindGroupLayoutEntry {
                    binding: 1,
                    visibility: wgpu::ShaderStages::COMPUTE,
                    ty: wgpu::BindingType::Texture {
                        multisampled: false,
                        sample_type: wgpu::TextureSampleType::Float { filterable: false },
                        view_dimension: wgpu::TextureViewDimension::D2,
                    },
                    count: None,
                },   // Write computed image to this texture
                wgpu::BindGroupLayoutEntry {
                    binding: 2,
                    visibility: wgpu::ShaderStages::COMPUTE,
                    ty: wgpu::BindingType::StorageTexture {
                        access: wgpu::StorageTextureAccess::WriteOnly,
                        format: wgpu::TextureFormat::Rgba32Float,
                        view_dimension: wgpu::TextureViewDimension::D2,
                    },
                    count: None,
                },
            ],
        });
    }

    fn create_render_bind_group_layout(&self) -> BindGroupLayout {
        return self.device.create_bind_group_layout(&wgpu::BindGroupLayoutDescriptor {
            label: Some("rended_compute_bind_group_layout"),
            entries: &[
                wgpu::BindGroupLayoutEntry {
                    binding: 0,
                    visibility: wgpu::ShaderStages::FRAGMENT,
                    ty: wgpu::BindingType::Texture {
                        multisampled: false,
                        sample_type: wgpu::TextureSampleType::Float { filterable: false },
                        view_dimension: wgpu::TextureViewDimension::D2,
                    },
                    count: None,
                },
            ],
        });
    }

    fn create_compute_pipeline_layout(&self, compute_bgl: &BindGroupLayout) -> PipelineLayout {
        return self.device.create_pipeline_layout(&wgpu::PipelineLayoutDescriptor {
            label: None,
            bind_group_layouts: &[compute_bgl],
            push_constant_ranges: &[],
        });
    }

    fn create_render_pipeline_layout(&self, render_bgl: &BindGroupLayout) -> PipelineLayout {
        return self.device.create_pipeline_layout(&wgpu::PipelineLayoutDescriptor {
            label: None,
            bind_group_layouts: &[render_bgl],
            push_constant_ranges: &[],
        });
    }

    fn create_compute_bind_group(
        &self, bgl: &BindGroupLayout, uniform: &Buffer, t1: &Texture, t2: &Texture
    ) -> BindGroup {
        return self.device.create_bind_group(&wgpu::BindGroupDescriptor {
            label: Some("create_compute_bind_group"),
            layout: bgl,
            entries: &[
                wgpu::BindGroupEntry {
                    binding: 0,
                    resource: uniform.as_entire_binding(),
                },
                wgpu::BindGroupEntry {
                    binding: 1,
                    resource: BindingResource::TextureView(&t1.create_view(&Default::default())),
                },
                wgpu::BindGroupEntry {
                    binding: 2,
                    resource: BindingResource::TextureView(&t2.create_view(&Default::default())),
                },
            ],
        });
    }

    fn create_render_bind_group(&self, render_bgl: &BindGroupLayout, t: &Texture) -> BindGroup {
        return self.device.create_bind_group(&wgpu::BindGroupDescriptor {
            label: Some("create_render_bind_group"),
            layout: render_bgl,
            entries: &[
                wgpu::BindGroupEntry {
                    binding: 0,
                    resource: BindingResource::TextureView(&t.create_view(&Default::default())),
                },
            ],
        });
    }

    fn create_render_shader(&self) -> ShaderModule {
        return self.device.create_shader_module(wgpu::ShaderModuleDescriptor {
            label: None,
            source: wgpu::ShaderSource::Wgsl(include_str!("render.wgsl").into()),
        });
    }

    fn create_compute_shader(&self) -> ShaderModule {
        return self.device.create_shader_module(wgpu::ShaderModuleDescriptor {
            label: None,
            source: wgpu::ShaderSource::Wgsl(
                include_str!("compute.wgsl").into()
            ),
        });
    }

    fn create_compute_pipeline(&self, cpll: PipelineLayout, sm: &ShaderModule) -> ComputePipeline {
        return self.device.create_compute_pipeline(&wgpu::ComputePipelineDescriptor {
            label: None,
            layout: Some(&cpll),
            module: sm,
            entry_point: "main",
        });
    }

    fn create_render_pipeline(&self, pll: PipelineLayout, sm: &ShaderModule) -> RenderPipeline {
        return self.device.create_render_pipeline(&wgpu::RenderPipelineDescriptor {
            label: Some("create_render_pipeline"),
            layout: Some(&pll),
            vertex: wgpu::VertexState {
                module: sm,
                entry_point: "vs_main",
                buffers: &[],
            },
            fragment: Some(wgpu::FragmentState {
                module: sm,
                entry_point: "fs_main",
                targets: &[Some(self.config.format.into())],
            }),
            primitive: wgpu::PrimitiveState::default(),
            depth_stencil: None,
            multisample: wgpu::MultisampleState::default(),  // default disables anti-aliasing
            multiview: None,
        });
    }

    fn create_render_bundle(&self, bg: &BindGroup, rp: &RenderPipeline) -> RenderBundle {
        let parameters = wgpu::RenderBundleEncoderDescriptor {
            label: None,
            color_formats: &[Some(self.config.format)],
            depth_stencil: None,
            sample_count: 1,
            multiview: None,
        };
        let mut e = self.device.create_render_bundle_encoder(&parameters);
        e.set_pipeline(rp);
        e.set_bind_group(0, bg, &[]);
        // Even though no vertex buffers were given, we can still invoke the vertex shader.
        e.draw(0..3, 0..1);  // We use 3 calls to fill screen with one oversized triangle.
        return e.finish(&wgpu::RenderBundleDescriptor{label: None});
    }

    fn write_buffer<T: bytemuck::Pod>(&self, buf: &Buffer, data: &T) {
        self.queue.write_buffer(buf, 0, bytemuck::bytes_of(data));
    }

    #[allow(unused_variables)]
    #[cfg(target_arch = "wasm32")]
    fn read_texture_to_png(&self, image_read_buf: &Buffer, res: UVec2, postfix: &str) {
        return;  // Web has no file system to write image into
    }

    #[cfg(not(target_arch = "wasm32"))]
    fn read_texture_to_png(&self, image_read_buf: &Buffer, res: UVec2, postfix: &str) {
        let bytes_per_row = res.x as usize * BYTES_PER_PIXEL;
        let bytes_per_row_padded = (((bytes_per_row - 1) / 256) + 1) * 256;

        // Note that we're not calling `.await` here.
        let buffer_slice = image_read_buf.slice(..);
        let (sender, receiver) = futures_intrusive::channel::shared::oneshot_channel();
        buffer_slice.map_async(
            wgpu::MapMode::Read,
            move |v| sender.send(v).unwrap()
        );

        // Poll the device in a blocking manner so that our future resolves.
        // In an async application, `device.poll(...)` should be called in an event loop
        self.device.poll(wgpu::Maintain::Wait);
        // Pollster is an minimal async executor that can only evaluate a future and nothing more
        if let Some(Ok(())) = pollster::block_on(receiver.receive()) {
            let padded_buffer = buffer_slice.get_mapped_range();
            let img = padded_buffer
                .chunks_exact(bytes_per_row_padded)
                .map(|row| {
                    row[0..res.x as usize * BYTES_PER_PIXEL] // remove padding
                    .chunks_exact(BYTES_PER_PIXEL)  // convert float-RGBA to u8-RGB
                    .map(|pixel| {
                        let ray_count = f32::from_le_bytes(pixel[12..16].try_into().unwrap());
                        let rgb = glam::Vec3::new(
                            f32::from_le_bytes(pixel[0..4].try_into().unwrap()),
                            f32::from_le_bytes(pixel[4..8].try_into().unwrap()),
                            f32::from_le_bytes(pixel[8..12].try_into().unwrap())
                        ) / ray_count;
                        let rgb = rgb.powf(0.4545); // Gamma correction for SRGB color space of PNG
                        (rgb * 255.0).as_ref().map(|v| v as u8)  // return [u8; 3]
                    }).flatten()
                }).flatten()
                .collect::<Vec<u8>>();
            let mut number = 0;
            for entry in std::fs::read_dir("./").unwrap() {
                // Extract number from filename, if there's one
                if let Some(num) = entry.map(|path| path.path()).ok()
                    .filter(|path| path.is_file())
                    .and_then(|path| path.file_name().map(|n| n.to_owned()))
                    .and_then(|filename| filename.to_str().map(|f| f.to_owned()))
                    .filter(|name| name.starts_with("out") && name.ends_with(".png"))
                    .and_then(|name| name.chars().position(|c| c=='-')
                        .and_then(|i| name.get(3..i).map(|n| n.to_owned()))
                    )
                    .and_then(|num_str| num_str.parse::<u32>().ok())
                {  // Clippy wanted formatting like this, so dunno, ok then
                    number = number.max(num);
                }
            }
            assert_eq!(img.len(), (res.x*res.y*3) as usize);
            let name = format!("out{}-{}.png", number+1, postfix);
            save_buffer(&name, &img, res.x, res.y, Rgb8).unwrap();
            println!("Saved image as {}", &name);

            // With the current interface, we have to make sure all mapped views are
            // dropped before we unmap the buffer.
            drop(padded_buffer);

            image_read_buf.unmap();
        }
    }

    fn render(
        &self,
        cpl: &ComputePipeline,
        cbg: &BindGroup,
        rb: &RenderBundle,
        res: UVec2,
        texture_read: Option<(&Texture, &Buffer)>,  // If Some, then write image texture to buffer
    ) {
        let frame = match self.surface.get_current_texture() {  // Return image of swapchain
            Ok(frame) => frame,
            Err(_) => {
                self.surface.configure(&self.device, &self.config);
                self.surface.get_current_texture().expect("Failed to get next surface texture!")
            }
        };
        let view = frame.texture.create_view(&wgpu::TextureViewDescriptor::default());

        let mut encoder = self.device.create_command_encoder(
            &wgpu::CommandEncoderDescriptor { label: None }
        );
        {
            let mut cpass = encoder.begin_compute_pass(&Default::default());
            cpass.set_pipeline(cpl);
            cpass.set_bind_group(0, cbg, &[]);
            // This is number of workgroups. The work group size is set to 16×16 in 'compute.wgls'
            cpass.dispatch_workgroups(res.x / 16 + 1, res.y / 16 + 1, 1);
        }
        {
            let mut rpass = encoder.begin_render_pass(&wgpu::RenderPassDescriptor {
                label: None,
                color_attachments: &[Some(wgpu::RenderPassColorAttachment {
                    view: &view,
                    resolve_target: None,
                    // Screen not cleared before every draw
                    ops: wgpu::Operations {load: wgpu::LoadOp::Load, store: true},
                })],
                depth_stencil_attachment: None,
            });
            rpass.execute_bundles(std::iter::once(rb));
        }
        // Copy the image from the texture to the buffer if user wants to write it to png
        if let Some((tex, output_buffer)) = texture_read {
            let bytes_per_row = res.x as usize * BYTES_PER_PIXEL;
            let bytes_per_row_padded = (((bytes_per_row - 1) / 256) + 1) * 256;
            encoder.copy_texture_to_buffer(
                tex.as_image_copy(),
                wgpu::ImageCopyBuffer {
                    buffer: output_buffer,
                    layout: wgpu::ImageDataLayout {
                        offset: 0,
                        bytes_per_row: Some(NonZeroU32::new(bytes_per_row_padded as u32).unwrap(),
                        ),
                        rows_per_image: None,
                    },
                },
                wgpu::Extent3d {width: res.x, height: res.y, depth_or_array_layers: 1,},
            );
        }

        self.queue.submit(Some(encoder.finish()));
        frame.present();
    }
}

