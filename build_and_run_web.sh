#!/bin/sh
# This builds and runs in web browser. If you want to do it locally, then run just "cargo run".

# Installation:
#####################################
# Install the version of wasm-bindgen matching the cargo.toml
# cargo install -f wasm-bindgen-cli --version 0.2.79

# Install wasm backend
# rustup target add wasm32-unknown-unknown

# Also to run you need to enable in broweser setting
# Firefox Nightly:
#   Go to about:config, and set: dom.webgpu.enabled to True
# Chromium:
#   Go to about://flags and enable enable-unsafe-webgpu
#   Linux: Also start program with flag --enable-features=Vulkan
#

# Also, you may need to install vulkan drivers, run (on ubuntu)
# TODO not sure, is it required
# sudo apt-get install build-essential git python cmake libvulkan-dev vulkan-utils
#####################################

# The command "set -ex" does two things:
#   1. Prints every command.
#   2. Does not continue running script if some command fails
set -ex  

# Build
# Use "--no-default-features" to disable unused "cross" feature
RUSTFLAGS=--cfg=web_sys_unstable_apis cargo build --release --target wasm32-unknown-unknown --no-default-features
# Generate javascript files and bindings in a `target/web` directory
wasm-bindgen --out-dir target/web --web ./target/wasm32-unknown-unknown/release/simple_path_tracer.wasm

# Start web server. It just sends the web page ('index.html') to receiver.
python3 -m http.server
# I don't know why the app does not run by opening 'index.html' directly in browser.

# So you can access the app via: http://0.0.0.0:8000/
